var configs = {};
var s = window.localStorage;
var options = ['#ededed','#ff0000','#0015ff', '#039625', '#055f75', '#5c510d'];

Object.prototype.fetch = function(id) {
  var kls = Object.keys(this);
  for(var i in kls) {
    if (kls[i] == id) {
      return this[kls[i]];
    }
  }
  return [id, 0,'not set',''];
};

$(document).ready(function(){
  var cache = s.getItem('config');
  if (cache !== null) {
    configs = JSON.parse(cache);
  }
  loadList();
  $('table.bz_buglist').delegate('.status-tag', 'hover', function(e){
    if (e.type == 'mouseover') {
      var id = $(this).attr('data-id');
      var config = configs.fetch(id);
      $('#helper_comment').text(config[3]).show();
    } else {
      $('#helper_comment').hide();
    }
  });
  $('table.bz_buglist').delegate('.status-tag', 'click', function(){
    var id = $(this).attr('data-id');
    if (!id) return;
    $('#modal.bzhelper').attr('data-id',id).show();
    var config = configs.fetch(id);
    $('#modal.bzhelper #desc').val(config[2]);
    $('#modal.bzhelper #comment').val(config[3]);
    $('#modal.bzhelper .item.tags span').removeClass('cur');
    $('#modal.bzhelper .item.tags span').eq(parseInt(config[1])).addClass('cur');
  });
  buildWindow();
});

function buildWindow() {
  var tags = '';
  for(var i in options) {
    if (typeof options[i] !== 'string') continue;
    tags = tags + '<span style="background:' + options[i] + ';" data-tag-id="'+i+'">&nbsp;</span>';
  }
  tags = '<div class="item tags">' + tags + '</div>';
  var desc = '<div class="item"><input type="text" id="desc"></div>';
  var comment = '<div class="item"><textarea id="comment"></textarea></div>';
  var savebtn = '<div class="item"><button type="button" id="save">Save</button></div>';
  var modal = '<div id="modal" class="bzhelper">' + tags + desc + comment + savebtn + '</div>';
  $(document).find('body').append(modal);
  $(document).find('body').append('<div id="helper_comment">e</div>');
  $('.bzhelper').css('left',$(document).width()/2 - 240);
  $('.bzhelper').css('top',$(window).height()/2 - 100);
  $('#modal.bzhelper .item.tags span').click(function(){
    $('#modal.bzhelper .item.tags span').removeClass('cur');
    $(this).addClass('cur');
  });
  $('#modal.bzhelper').delegate('button', 'click', function(){
    var id = $('.bzhelper').attr('data-id');
    var tagid = $('.bzhelper .tags .cur').attr('data-tag-id');
    var desc = $('.bzhelper #desc').val();
    var comment = $('.bzhelper #comment').val();
    var config = configs.fetch(id);
    configs[config[0]] = [config[0], tagid, desc, comment];
    s.setItem('config', JSON.stringify(configs));
    $('#modal.bzhelper').hide();
    loadList();
  });
}

function loadList() {
  var trlist = $('table.bz_buglist tr');
  var id;
  trlist.each(function(){
    if ($(this).attr('id')) {
      id = $(this).attr('id');
      var config = configs.fetch(id);
      var a = $(this).find('td.bz_short_desc_column a')[0].outerHTML;
      var buildtag = 
          (config[1] == 0) ?
          ('<font class="status-tag" style="padding:0 3px;background:#fff;color:#000;cursor:pointer;" data-id="'+id+'">' + config[2] + '</font>') :
          ('<font class="status-tag" style="padding:0 3px;background:'+options[config[1]]+';color:#fff;cursor:pointer;" data-id="'+id+'">' + config[2] + '</font>')
      $(this).find('td.bz_short_desc_column').html(buildtag+a);
    }
  });
}